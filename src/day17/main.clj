(ns day17.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.data.priority-map :refer [priority-map priority-map-keyfn]]))


(defn indexed [xs]
  (map-indexed vector xs))

(defn read-input [filename]
  (into {}
        (for [[y line] (indexed (str/split-lines (slurp (io/resource filename))))
              [x heat] (indexed line)]
          [[x y] (parse-long (str heat))])))

(def full-input (read-input "day17/input17.txt"))
(def sample1    (read-input "day17/sample01.txt"))


(def DOWN  [0 1])
(def UP    [0 -1])
(def LEFT  [-1 0])
(def RIGHT [1 0])

(def opposite-dir
  {UP DOWN
   DOWN UP
   LEFT RIGHT
   RIGHT LEFT})

(defn apply-dir [[x y] [dx dy]]
  [(+ x dx)
   (+ y dy)])

(defn move-pos [pos dir n]
  (apply-dir pos
             (map (partial * n) dir)))

(defn move [input [pos hv :as state] heat dir n]
  (let [pos' (move-pos pos dir n)
        hv'  (if (zero? hv) 1 0)]
    (when (input pos')
      [[pos' hv']
       (apply + heat
              (for [offset (range 1 (inc n))]
                (input (move-pos pos dir offset))))])))


(defn search [input steps]
  (let [start [0 0]
        end (last (sort (keys input)))]
    (loop [states (priority-map [start 0] 0
                                [start 1] 0)
           best {}
           n 0]
      (let [[[pos hv :as state] heat] (first states)]
        #_(when (= 0 (mod n 10000))
          (println (format "[%d] %s/%s %s %s" n heat (best state "-") pos hv))
          (Thread/sleep 1))

        (cond
          (nil? state)
          nil

          (= end pos)
          heat

          :else
          (let [next-states (for [dir (if (= hv 1) [UP DOWN] [LEFT RIGHT])
                                  n steps
                                  :let [next-pair (move input state heat dir n)]
                                  :when next-pair
                                  :let [[state' heat'] next-pair]
                                  :when (< heat' (get states state' Long/MAX_VALUE))
                                  :when (< heat' (best state' Long/MAX_VALUE))]
                              next-pair)]

            (recur (into (pop states) next-states)
                   (assoc best state heat)
                   (inc n))))))))

(defn part1 [input]
  (search input [1 2 3]))


(defn part2 [input]
  (search input (range 4 11)))

(comment
  (part1 sample1)
  102
  (part1 full-input)
  942

  (part2 sample1)
  94
  (part2 full-input)
  1082)

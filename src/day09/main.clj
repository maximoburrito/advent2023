(ns day09.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(defn parse-line [text]
  (mapv parse-long (str/split (str/trim text) #" ")))

(defn read-input [filename]
  (into []
        (for [line (str/split-lines (slurp (io/resource filename)))]
          (parse-line line))))

(def full-input  (read-input "day09/input09.txt"))
(def sample1 (read-input "day09/sample01.txt"))


(defn sum [ns] (reduce + 0 ns))

(defn dseq [ns]
  (map (fn [[a b]] (- b a))
       (partition 2 1 ns)))

(defn next-in-seq [ns]
  (loop [ns ns
         sum 0]
    (if (every? zero? ns)
      sum
      (recur (dseq ns)
             (+ sum (last ns))))))

(defn part1 [input]
  (sum
    (map next-in-seq input)))


(defn prev-in-seq [ns]
  (if (every? zero? ns)
    0
    (- (first ns)
       (solve2 (dseq ns)) )))

(defn part2 [input]
  (sum
    (map prev-in-seq input)))

(comment
  (part1 sample1)
  114
  (part1 full-input)
  2075724761

  (part2 sample1)
  2
  (part2 full-input)
  1072)

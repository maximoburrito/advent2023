(ns day07.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(def FIVE-KIND  7)
(def FOUR-KIND  6)
(def FULL-HOUSE 5)
(def THREE-KIND 4)
(def TWO-PAIR   3)
(def ONE-PAIR   2)
(def HIGH-CARD  1)

(defn parse-line [text]
  (let [[cards bid] (str/split text #"\W+")]
    {:cards (into [] cards)
     :bid (parse-long bid)}))

(defn read-input [filename]
  (mapv parse-line (str/split-lines (slurp (io/resource filename)))))

(def full-input (read-input "day07/input07.txt"))
(def sample1 (read-input "day07/sample01.txt"))

(defn hand-strength [{:keys [cards]}]
  (let [freqs (frequencies cards)
        counts (set (vals freqs))]
    (cond
      (counts 5)
      FIVE-KIND

      (counts 4)
      FOUR-KIND

      (and (counts 3) (counts 2))
      FULL-HOUSE

      (counts 3)
      THREE-KIND

      (= 2 (count (filter #(= 2 %) (vals freqs)))) ;; two pair
      TWO-PAIR

      (counts 2)
      ONE-PAIR

      :else
      HIGH-CARD)))


(defn sum [ns] (reduce + 0 ns))

(defn winnings [ordered-hands]
  (sum
    (for [[n hand] (map-indexed vector ordered-hands)]
      (* (inc n) (:bid hand)))))

(def card-values-simple
  {\2 2
   \3 3
   \4 4
   \5 5
   \6 6
   \7 7
   \8 8
   \9 9
   \T 10
   \J 11
   \Q 12
   \K 13
   \A 14})

(defn sort-key-simple [hand]
  [(hand-strength hand) (mapv card-values-simple (:cards hand))])

(defn part1 [input]
  (winnings
    (sort-by sort-key-simple input)))


;; ----------------------------------------
;; part2
(def card-values-jokers-wild
  {\2 2
   \3 3
   \4 4
   \5 5
   \6 6
   \7 7
   \8 8
   \9 9
   \T 10
   \J 1  ;; lowest value
   \Q 12
   \K 13
   \A 14})

(defn sort-key-wild [hand]
  [(hand-strength-wild hand) (mapv card-values-jokers-wild (:cards hand))])

(defn joker? [card]
  (= \J card))

(defn hand-strength-wild [{:keys [cards]}]
  (let [jokers (count (filter joker? cards))
        basic-hand (remove joker? cards)
        basic-strength (hand-strength {:cards basic-hand})]
    (cond
      (= 0 jokers)
      basic-strength

      (= 1 jokers)
      (cond
        (= FOUR-KIND basic-strength)
        FIVE-KIND
        (= THREE-KIND basic-strength)
        FOUR-KIND
        (= TWO-PAIR basic-strength)
        FULL-HOUSE
        (= ONE-PAIR basic-strength)
        THREE-KIND
        (= HIGH-CARD basic-strength)
        ONE-PAIR
        :else (throw (ex-info "nope1" {:cards cards})))

      (= 2 jokers)
      (cond
        (= THREE-KIND basic-strength)
        FIVE-KIND
        (= ONE-PAIR basic-strength)
        FOUR-KIND
        (= HIGH-CARD basic-strength)
        THREE-KIND
        :else (throw (ex-info "nope2" {:cards cards})))

      (= 3 jokers)
      (cond
        (= ONE-PAIR basic-strength)
        FIVE-KIND
        (= HIGH-CARD basic-strength)
        FOUR-KIND
        :else (throw (ex-info "nope3" {:cards cards})))

      (or (= 5 jokers)
          (= 4 jokers))
      FIVE-KIND

      :else
      (throw (ex-info "nope4" {:cards cards})))))

(defn part2 [input]
  (winnings
    (sort-by sort-key-wild input)))

#_(defn inspect [cards]
  (doseq [hand (sort-by sort-key-wild cards)]
    (println (format "[%s] %s" (hand-strength-wild hand) (apply str (:cards hand))))))

(comment
  (part1 sample1)
  6440
  (part1 full-input)
  253910319

  (part2 sample1)
  5905
  (part2 full-input)
  254083736)

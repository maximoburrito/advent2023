(ns day12.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(def UNKNOWN \?)
(def EMPTY   \.)
(def DAMAGED \#)

(defn parse-line [line]
  (let [[l r] (str/split line #" ")]
    {:report l
     :counts (mapv parse-long (str/split r #"\,"))}))

(defn read-input [filename]
  (map parse-line
       (str/split-lines
        (slurp (io/resource filename)))))

(def full-input (read-input "day12/input.txt"))
(def sample1    (read-input "day12/sample01.txt"))

;; ---------------------------------------
(defn sum [ns]
  (reduce + ns))

(defn add-to-partials [partials item]
  (map #(str % item) partials))

(defn perms [report]
  (loop [report report
         partials  [""]]
    (cond
      (empty? report)
      partials

      (= UNKNOWN (first report))
      (recur (rest report)
             (concat (add-to-partials partials DAMAGED)
                     (add-to-partials partials EMPTY)))


      :else
      (recur (rest report)
             (add-to-partials partials (first report))))))

(defn score [report]
  (mapv count (re-seq #"[^\.]+" report)))


(defn matches [line]
  (count (filter true? (map #(= (:counts line) (score %)) (perms (:report line))))))

(defn part1 [input]
  (sum (map matches input)))

;; ----------------------------------------

(defn expand-input [input]
  (for [line input]
    (-> line
        (update :report #(str/join UNKNOWN (take 5 (repeatedly (constantly  %)))))
        (update :counts #(into []
                               (concat % % % % %))))))
(defn handle-empty [partials]
  (apply merge-with +'
         (for [[[n goals] count] partials]
           (cond
             ;; no accumulated, continue
             (= n 0)
             {[n goals] count}

             ;; accumulated matches goal, consume
             (= n (first goals))
             {[0 (vec (rest goals))] count}

             ;; too few or too many, cancelt
             :else nil))))

(defn handle-damaged [partials]
  (apply merge-with +'
         (for [[[n goals] count] partials
               :when (seq goals)
               :when (< n (first goals))]
           {[(inc n) goals] count})))

(defn step [partials next-char]
  (cond
    (= EMPTY next-char)
    (handle-empty partials)

    (= DAMAGED next-char)
    (handle-damaged partials)

    (= UNKNOWN next-char)
    (merge-with +'
     (handle-damaged partials)
     (handle-empty partials))))

(defn perms2 [{:keys [report counts]}]
  (loop [remaining report
         partials  {[0 counts] 1}]
    (cond
      (empty? remaining) ;; make sure to clear out all goals
      (get (step partials EMPTY) [0 []])

      :else
      (recur (rest remaining)
             (step partials (first remaining))))))

(defn part2 [input]
  (sum (map perms2 (expand-input input))))


(comment
  (part1 sample1)
  21
  (part1 full-input)
  7460

  (part2 sample1)
  525152

  (part2 full-input)
  6720660274964)

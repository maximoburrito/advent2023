(ns day11.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(def GALAXY \#)
(def SPACE \.)

(defn indexed [xs]
  (map-indexed vector xs))

(defn read-input [filename]
  (into {}
        (for [[y line] (indexed (str/split-lines (slurp (io/resource filename))))
              [x spot] (indexed line)]
          [[x y] spot])))

(def full-input (read-input "day11/input.txt"))
(def sample1    (read-input "day11/sample01.txt"))
(def sample2    (read-input "day11/sample02.txt"))
(def sample3    (read-input "day11/sample03.txt"))

(defn find-galaxies [input]
  (filter #(not= SPACE (input %))
          (keys input)))

(defn expand [input [x y]]
  (let [dim (int (Math/sqrt (count input)))
        galaxies (find-galaxies input)
        xs (set (map first galaxies))
        ys (set (map second galaxies))]

    [(+ x (count (filter (complement xs) (range x))))
     (+ y (count (filter (complement ys) (range y))))]))

(defn distance [[x1 y1] [x2 y2]]
  (+ (abs (- x2 x1))
     (abs (- y2 y1))))

(defn sum [ns]
  (reduce + ns))

(defn part1 [input]
  (let [galaxies (map #(expand input %)
                      (filter #(= GALAXY (input %)) (keys input)))]

    (sum (for [g1 galaxies
               g2 galaxies
               :when (= 1 (compare g1 g2))]

           (distance g1 g2)))))

(defn big-expand [input factor [x y]]
  (let [dim (int (Math/sqrt (count input)))
        galaxies (find-galaxies input)
        xs (set (map first galaxies))
        ys (set (map second galaxies))]

    [(+ x (* (dec factor) (count (filter (complement xs) (range x)))))
     (+ y (* (dec factor) (count (filter (complement ys) (range y)))))]))

(defn part2 [input]
  (let [initial-galaxies (filter #(= GALAXY (input %)) (keys input))
        galaxies (map #(big-expand input 1000000 %) initial-galaxies)]
    (sum (for [g1 galaxies
               g2 galaxies
               :when (= 1 (compare g1 g2))]
           (distance g1 g2)))))
(comment
  (part1 full-input)

  (part2 full-input))

(ns day12.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(def ASH  \.)
(def ROCK \#)

(defn parse-grid [text]
  (str/split-lines text))

(defn read-input [filename]
  (map parse-grid
       (str/split
        (slurp (io/resource filename))
        #"\n\n")))

(def full-input (read-input "day13/input.txt"))
(def sample1    (read-input "day13/sample01.txt"))

;; ---------------------------------------

(defn sum [ns]
  (reduce + ns))

(defn find-symmetry [grid]
  (or (first
       (for [n (range 1 (count grid))
             :let [[h t] (split-at n grid)
                   pairs (map vector (reverse h) t)]
             :when (every? #(apply = %) pairs)]
         n))
      0))

(defn symmetry-score [grid]
  (+ (* 100 (find-symmetry grid))
     (find-symmetry (invert grid))))

(defn invert [grid]
  (into []
        (for [n (range (count (first grid)))]
          (apply str (map #(nth % n) grid)))))

(defn part1 [input]
  (sum
   (map symmetry-score input)))

;; ----------------------------------------

(defn toggle-spot [spot]
  (if (= ASH spot)
    ROCK
    ASH))

(defn toggle-grid [grid x y]
  (update grid y
          #(str (subs % 0 x)
                (toggle-spot (nth % x))
                (subs % (inc x)))))

(defn indexed [xs]
  (map-indexed vector xs))

(defn variants [grid]
  (for [[y line] (indexed grid)
        [x spot] (indexed line)]
    (toggle-grid grid x y)))

(defn find-symmetries [grid]
  (for [n (range 1 (count grid))
        :let [[h t] (split-at n grid)
              pairs (map vector (reverse h) t)]
        :when (every? #(apply = %) pairs)]
    n))

(defn symmetry-score-ignoring [grid ignored]
  (or
   (first
    (->> (find-symmetries grid)
         (map #(* 100 %))
         (filter #(not= ignored %))))
   (first
    (->> (find-symmetries (invert grid))
         (filter #(not= ignored %))))))

(defn alternative-score [grid]
  (let [base-score (symmetry-score grid)]
    (first
     (for [grid' (variants grid)
           :let [score (symmetry-score-ignoring grid' base-score)]
           :when score]
       score))))

(defn part2 [input]
  (sum (map alternative-score input)))



;; ----------------------------------------
(comment
  (part1 sample1)
  405
  (part1 full-input)
  33356

  (part2 sample1)
  400
  (part2 full-input)
  28475)

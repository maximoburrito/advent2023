;; first pass with no cleanup
;; I manually inspected the input to determine what the S piece turned into
;; (solve the input not the puzzle)
;; I should probably go back and actually write that part some time
;;
;; for part 2 I was really stumped, but ultimately I removed all the pipes
;; not int the cycle and then converted each point to a 3x3 grid so that I could
;; path find.

(ns day10.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))


(def START \S)
(def HORIZONTAL \-)
(def VERTICAL \|)
(def UPPER-LEFT \F)
(def UPPER-RIGHT \7)
(def LOWER-RIGHT \J)
(def LOWER-LEFT  \L)
(def GROUND \.)

(def UP [0 -1])
(def DOWN [0 1])
(def LEFT [-1 0])
(def RIGHT [1 0])

(def neighbor-delta
  {UPPER-LEFT  [RIGHT DOWN]
   UPPER-RIGHT [LEFT DOWN]
   LOWER-LEFT  [UP RIGHT]
   LOWER-RIGHT [UP LEFT]
   HORIZONTAL  [LEFT RIGHT]
   VERTICAL    [UP DOWN]})

(defn indexed [xs]
  (map-indexed vector xs))

(defn read-input [filename]
  (into {}
        (for [[y line] (indexed (str/split-lines (slurp (io/resource filename))))
              [x spot] (indexed line)]
          [[x y] spot])))

(def full-input (read-input "day10/input10.txt"))
(def sample1    (read-input "day10/sample01.txt"))
(def sample2    (read-input "day10/sample02.txt"))
(def sample3    (read-input "day10/sample03.txt"))




(defn find-start [pipes]
  (first
    (for [[pos spot] pipes
          :when (= spot START)]
      pos)))


(defn neighbors [input [x y :as pos]]
  (into []
        (for [[dx dy] (neighbor-delta (input pos))]
          [(+ x dx) (+ y dy)])))

(defn valid-neighbors [input pos]
  (for [neighbor (neighbors input pos)
        :when (contains? (set (neighbors input neighbor)) pos)]
    neighbor))



(defn part1 [original-input replacement]
  (let [start (find-start original-input)
        input (assoc original-input start replacement)]

    (loop [to-visit #{start}
           visited #{}
           n 0]
      #_(println "*" n to-visit )
      (if (empty? to-visit)
        (dec n)
        (let [next (for [pos to-visit
                         neighbor (valid-neighbors input pos)
                         :when (and (not (visited neighbor))
                                    (not (to-visit neighbor)))]
                     neighbor)]
          (recur (set next)
                 (into visited to-visit)
                 (inc n)))))))


(defn loop-points [input start ]
  (loop [to-visit #{start}
         visited #{}]
    (if (empty? to-visit)
      visited
      (let [next (for [pos to-visit
                       neighbor (valid-neighbors input pos)
                       :when (and (not (visited neighbor))
                                  (not (to-visit neighbor)))]
                   neighbor)]
        (recur (set next)
               (into visited to-visit))))))

(defn remove-dead-pipes [input live]
  (reduce #(if (contains? live %2)
             %1
             (assoc %1 %2 GROUND))
          input
          (keys input)))


(defn dump [input]
  (let [xmax (reduce max (map first (keys input)))
        ymax (reduce max (map second (keys input)))]
    (doseq [y (range (inc ymax))]
      (doseq [x (range (inc xmax))]
        (print (input [x y])))
      (println))))

(defn dump2 [points]
  (let [xmax (reduce max (map first points))
        ymax (reduce max (map second points))]
    (doseq [y (range (inc ymax))]
      (doseq [x (range (inc xmax))]
        (print (if (points [x y]) "." "*")))
      (println))))


(defn explode-input [input]
  (reduce into
           (for [[x y :as pos] (keys input)
                 :let          [spot (input pos)
                                x' (* 3 x)
                                y' (* 3 y)
                                deltas (set (neighbor-delta spot))]]
             (cond-> #{[x'  y']
                       [x' (+ y' 2)]
                       [(+ x' 2) y']
                       [(+ x' 2) (+ y' 2)]}

               (empty? deltas) ;; middle
               (conj [(+ x' 1) (+ y' 1)])

               (not (contains? (set (neighbor-delta spot)) UP))
               (conj [(+ x' 1) y'])

               (not (contains? (set (neighbor-delta spot)) DOWN))
               (conj [(+ x' 1) (+ y' 2)])

               (not (contains? (set (neighbor-delta spot)) LEFT))
               (conj [x' (+ y' 1)])

               (not (contains? (set (neighbor-delta spot)) RIGHT))
               (conj [(+ x' 2) (+ y' 1)])))))


(defn reachable [points start]
  (let [xmax (reduce max (map first points))
        ymax (reduce max (map second points))
        pneighbors (fn [[x y]]
                     (into []
                           (for [[dx dy] [UP DOWN LEFT RIGHT]
                                 :let [new-pos [(+ x dx) (+ y dy)]]
                                 :when (points new-pos)]
                             new-pos)))]
    (loop [to-visit #{start}
           visited #{}]
      (if (empty? to-visit)
        visited
        (let [visiting (first to-visit)
              neighbors-to-visit (for [neighbor (pneighbors visiting)
                                        :when (not (visited neighbor))]
                                    neighbor)]
          (recur (-> to-visit
                     (into neighbors-to-visit)
                     (disj visiting))
                 (conj visited visiting)))))))

(defn part2 [original-input replacement]
  (let [start    (find-start original-input)
        ;; replace the \S with the real piece, by inspection
        input    (assoc original-input start replacement)
        ;; remove the pipes that aren't part of the loop
        input2   (remove-dead-pipes input (loop-points input start))
        ;; convert the input grid to a 3x3 grid in a larger grid
        exploded (explode-input input2)
        ;; points in exploded 3x3 grid reachable from [0 0]
        outside  (reachable exploded [0 0])]

    ;; for each ground point in the original point, test if
    ;; the center point of it's position in the exploded
    ;; grid is reachable

    (count
      (for [[x y :as pos] (keys input2)
            :when         (= GROUND (input2 pos))
            :when         (not (outside [(inc (* x 3))
                                         (inc (* y 3))]))]
        pos))))


(comment
  (part1 sample1 UPPER-LEFT)
  4
  (part1 sample2 UPPER-LEFT)
  8
  (part1 full-input VERTICAL)
  6599

  (part2 sample3 UPPER-LEFT)
  4
  (part2 full-input VERTICAL)
  477)

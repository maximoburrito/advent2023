(ns day06.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))


(defn parse-line [line]
  (let [[_ ns] (re-matches #".*: (.*)" line)]
    (map parse-long (str/split (str/trim ns) #"\W+"))))

(defn read-input [filename]
  (let [[times distances]
        (str/split-lines (slurp (io/resource filename)))]
    (for [[time distance] (map vector (parse-line times) (parse-line distances))]
      {:t time :d distance})))


(def full-input (read-input "day06/input06.txt"))
(def sample1 (read-input "day06/sample01.txt"))


(defn traveled [{:keys [t d]} wind-up]
  (*' (- t wind-up) wind-up))

(defn wins? [race wind-up]
  (> (traveled race wind-up) (:d race)))

(defn part1 [input]
  (reduce * 1
          (for [race input]
            (count
              (for [n (range 1 (inc (:t race)))
                    :when (wins? race n)]
                n)))))

(defn de-kern [input]
  {:t (parse-long (apply str (map :t input)))
   :d (parse-long (apply str (map :d input)))})



(defn bsearch [l h pred]
  (println "*" l h)
  (if (= (inc l) h)
    l
    (let [mid (int (/ (+ l h) 2))]
      (if (pred mid)
        (recur l mid pred)
        (recur mid h pred)))))

(defn part2 [input]
  (let [real-input (de-kern input)
        ;; pick a time we know wins. I don't know if mathematically t/2 should necessarily
        ;; win, but it works for all the inputs I have
        known (int (/ (:t real-input) 2))]

    #_(println "sanity" known (wins? real-input known))

    ;; we know that min-t (0) doens't win and max-t doesn't win, but known does
    ;; binary search one the low side to find the border from lose to win
    ;; then search on the right for the border of win to lose
    (let [first-win (inc (bsearch real-input 1 known #(wins? real-input %)))
          last-win (bsearch real-input known (:t real-input ) #(not (wins? real-input %)))]
      (inc (- last-win first-win)))))



(comment
  (part1 sample1)
  288
  (part1 full-input)
  316800

  (part2 sample1)
  71503
  (part2 full-input)
  45647654)

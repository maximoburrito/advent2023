(ns day05.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

;; hard coded based on input - what is the highest number we'll see in any range
(def MAX 9999999999)

(defn parse-longs [text]
  (mapv parse-long (str/split text #" ")))

(defn parse-map [lines]
  (mapv parse-longs (rest lines)))

(defn read-input [filename]
  (let [[seeds & maps] (map str/split-lines
                            (str/split (slurp (io/resource filename)) #"\n\n"))]
    {:seeds (parse-longs (last (str/split (first seeds) #": ")))
     ;; how many maps can map map map if map map could map maps
     :maps (map parse-map maps)}))


(def full-input (read-input "day05/input05.txt"))
(def sample1 (read-input "day05/sample01.txt"))

(defn sum [ns]
  (reduce + 0 ns))

(defn apply-map [n xy-map]
  (or (first (for [[dst src r] xy-map
                   :when (<= src n (+ r src -1))]
               (+ n (- dst src))))
      n))

(defn apply-maps [n xy-maps]
  (reduce apply-map n xy-maps))


(defn part1 [input]
  (reduce min
          (map #(apply-maps % (:maps input)) (:seeds input))))

(defn translate-one-chunk [n xy-map]
  ;; takes a number and a mapping and returns [n' r]
  ;; where n' is the start of the translated block and r is the number of elements in that chunk that we can transform
  (or
    ;; are we inside an existing block? If so, translate as much as we can from that block
    (first
        (for [[dst src r] xy-map
              :when (<= src n (+ r src -1))]
          [(+ n (- dst src))
           (- r (- n src))]))
    ;; we aren't in a chunk - is there chunk that is higher than us?
    ;; is so, return [n' r]  where n' = n (no translation) and r is how many we can
    ;; translate before the next blockx
    (first
      (for [[dst src r] (sort-by second xy-map)
            :when (> src n)]
        [n (- src n)]))

    ;; we are at the end. translate n->n for as many MAX items, which will cover any request
    [n MAX]))

(defn translate-range [[n r] amap]
  (let [[n' r']  (translate-one-chunk n amap)
        consumed (min r r')]
    ;; we want to translate [n r] and get back [n' r'] indicating
    ;; we can translate n->n' for the next r' items.
    (if (>= r' r)
      ;; if r' satisfies r, return a single range for the mapping
      [[n' r]]
      ;; if r' does not satisfy r,
      ;; make a chunk of what we can translate and recursively compute the translation of
      ;; the remaining chunk(s)
      (concat [[n' r']]
              (translate-range [(+ n r') (- r r')] amap)))))


(defn part2 [input]
  ;; this is a bit opaque, but the basic idea is that for each input range,
  ;; compute the all the ranges it could be after going through the each map
  ;; the lowest starting point after the final map is the winner...
  (let [final-ranges
        (reduce (fn [ranges xy-map]
                  (mapcat
                    (fn [range]
                      (translate-range range xy-map))
                    ranges))
                (partition 2 (:seeds input))
                (:maps input))
        smallest-chunk (first (sort-by first final-ranges))
        [start _] smallest-chunk]
    start))


(comment
  (part1 sample1)
  35
  (part1 full-input)
  218513636

  (part2 sample1)
  46
  (part2 full-input)
  81956384)

(ns day02.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(defn parse-roll [roll]
  (let [draws (str/split roll #", ")]
    (into {:red 0
           :blue 0
           :green 0}
          (for [draw draws
                :let [[n color] (str/split draw #" ")]]
            [(keyword color) (parse-long n)]))))

(defn parse-line [line]
  (let [[_ n games] (re-matches #"Game (.*): (.*)" line)
        rolls (str/split games #"; ")]
    {:n (parse-long n)
     :rolls (mapv parse-roll rolls)}))

(defn read-input [filename]
  (for [line (str/split-lines (slurp (io/resource filename)))]
    (parse-line line)))

(def full-input (read-input "day02/input02.txt"))
(def test-input (read-input "day02/test02.txt"))

(defn sum [ns]
  (reduce + 0 ns))

(defn possible-game? [game max]
  (every? true?
          (for [roll (:rolls game)]
            (and (<= (:red roll) (:red max))
                 (<= (:blue roll) (:blue max))
                 (<= (:green roll) (:green max))))))

(defn part1 [input]
  (sum
    (for [game input
          :when (possible-game? game {:red 12 :green 13 :blue 14})]
      (:n game))))

(defn part2 [input]
  (sum
    (for [game input]
      (let [required (reduce (partial merge-with max) (:rolls game))
            power (* (:red required) (:blue required) (:green required))]
        power))))


(comment
  (part1 full-input)
  1734

  (part2 full-input)
  70387)

(ns day08.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))


(defn parse-line [text]
  (let [ [_ a b c] (re-matches #"(...) = \((...), (...)\)"text)]
    [a {:l b :r c}]))

(defn read-input [filename]
  (let [lines (str/split-lines (slurp (io/resource filename)))]
    {:steps (map #(keyword (str/lower-case (str %))) (first lines))
     :tree (into {} (map parse-line (drop 2 lines)))}))

(def full-input  (read-input "day08/input08.txt"))
(def sample1 (read-input "day08/sample01.txt"))
(def sample2 (read-input "day08/sample02.txt"))

(defn sum [ns] (reduce + 0 ns))

(defn part1 [input]
  (loop [steps (cycle (:steps input))
         n 0
         state "AAA"]
    (if (= state "ZZZ")
      n
      (recur (rest steps)
             (inc n)
             (get-in input [:tree state (first steps)])))))



(defn goal-state? [state]
  (str/ends-with? state "Z"))

(defn start-state? [state]
  (str/ends-with? state "A"))

;; this didn't work, but I had to try ...
(defn part2-brute [input]
  (let [take-step (fn [state step]
                    (get-in input [:tree state step]))]
    (loop [steps (cycle (:steps input))
           n 0
           states (filter start-state? (keys (:tree input)))]

      (when (= 0 (mod n 1000000))
        (println n states))

      (if (every? goal-state? states)
        n
        (recur (rest steps)
               (inc n)
               (mapv #(take-step % (first steps)) states))))))

;; I wrote this to explore what the paths looked like
(defn explore [input state]
    (let [take-step (fn [state step]
                      (get-in input [:tree state step]))]
      (loop [steps (cycle (:steps input))
             n 0
             state state
             seen #{}]

        (when (goal-state? state)
          (println "*" n state))

        (if (and (goal-state? state)
                 (seen state))
          :repeat

          (recur (rest steps)
                 (inc n)
                 (take-step state (first steps))
                 (cond-> seen
                   (goal-state? state) (conj state)))))))


;; solve any input to goal-state
(defn solve [input start-state]
    (loop [steps (cycle (:steps input))
           n 0
           state start-state]
      (if (goal-state? state)
        n
        (recur (rest steps)
               (inc n)
               (get-in input [:tree state (first steps)])))))

(defn lazy-lcm [x y]
  (loop [n x]
    (if (zero? (mod n y))
      n
      (recur (+ n x)))))

;; since I could see each path starting node went to a final node and repeated
;; then calculate all the lengths and find the LCM
(defn part2 [input]
  (let [distances (for [state (keys (:tree input))
                        :when (start-state? state)]
                    (solve input state))]
    (reduce lazy-lcm distances)))

(comment
  (part1 sample1)
  2
  (part1 full-input)
  19099

  (part2 sample2)
  6
  (part2 full-input)
  17099847107071)

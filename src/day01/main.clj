(ns day01.main
  (:require [clojure.string :as str]
            [clojure.pprint :refer [cl-format]]
            [clojure.java.io :as io]))

;; input and processing
(defn read-input [filename]
  (str/split-lines (slurp (io/resource filename))))

(def full-input (read-input "day01/input01.txt"))


(defn char-is-digit? [c]
  (<= (int \0) (int c) (int \9)))

(defn only-numbers [text]
  (apply str (filter char-is-digit? text)))

(defn only-first-last [text]
  (str (first text) (last text)))

(defn calibration-value [text]
  (-> text
      only-numbers
      only-first-last
      parse-long))

(defn sum [ns]
  (reduce + 0 ns))

(defn part1 [input]
  (sum (map calibration-value input)))


(def patterns
  (reduce merge
          (for [n (range 1 10)]
            {(str n) n
             (cl-format nil "~R" n) n})))
(def reverse-patterns
  (reduce merge
          (for [n (range 1 10)]
            {(str n) n
             (str/reverse (cl-format nil "~R" n)) n})))

(defn first-number [text pattern-map]
  (or (first
        (for [[pattern n] pattern-map
              :when (str/starts-with? text pattern)]
          n))
      (when (not-empty text)
        (recur (.substring text 1) pattern-map))))

(defn calibration-value2 [text]
  (let [x (first-number text patterns)
        y (first-number (str/reverse text) reverse-patterns)]
    (+ y (* 10 x))))

(defn part2 [input]
  (sum (map calibration-value2 input)))


(comment
  (part1 full-input)
  55447

  (part2 full-input)
  54706)
